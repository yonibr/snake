package snake;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;

// This class runs the snake game. It is a Subclass of JPanel, a class form the
// Java Swing library which you probably want to look up. It implements the
// KeyListener interface, which works with Swing to keep track of when keys
// are pressed.
public class Snake extends JPanel implements KeyListener
{
    // Constants.
    // Note that multiple variables of the same types and modifiers (such as
    // public, static, and final in this case) can be declared in the same
    // statement. Some people like this, some people don't. Also note that a
    // statement can occur over multiple lines and is only terminated by a
    // semicolon (with the exception of blocks, which you can look up when you
    // get the chance). Finally, note that comments can occur in the middle of
    // statements. This is interesting, but not often used, as far as I've seen.
    // I'm just doing it to make it easier for you to understand everything.

    public static final int WIDTH = 640, // Window width
            // Window height
            HEIGHT = 480,
            // Size of a "grid block" in pixels. A grid block is an arbitrary
            // square of n by n pixels, representing the basic unit of movement.
            GRID_BLOCK_SIZE = 16,

            // x-coordinate of center of window
            CENTER_X = WIDTH / 2,

            // y-coordinate of center of window
            CENTER_Y = HEIGHT / 2,

            // Milliseconds between calculations
            MILLI_BETWEEN_CALCS = 100;

    // Creates a new Character object, and assigns it to the variable character.
    // The character is the actual snake.
    private Character character = new Character();

    // Creates a new Food object at a random location on the screen (as
    // implemented in the createFood() method of the Food class
    private Food food = Food.createFood();

    private int score = 0;

    // Variable to keep track of if the game is over
    private boolean gameOver = false,

            // Keeps track of if a key has been pressed during this calculation
            // cycle. This is to prevent multiple button presses during one
            // cycle which can result in illogical collisions. If you want to
            // see what happens, prevent this from ever being set to true later
            // in the code and see what happens when you press buttons quickly.
            keyPressed = false;

    // Constructor for the Snake class. Called when a new Snake object is
    // created.
    public Snake()
    {
        // Creates a new instance of JFrame, a Swing class which basically
        // represents a window.
        JFrame frame = new JFrame("Snake");

        // Sets the window bounds
        frame.setBounds(0, 0, WIDTH, HEIGHT);

        // Adds Snake to the window. This is possible because Snake is a
        // subclass of JPanel (which is a subclass of JComponent), and therefore
        // can be added to a JFrame.
        frame.add(this);

        // Create a new Timer object to be used to schedule repeated updates to
        // the game. The basic idea of a game is that you want something called
        // a game loop. A game loop is a loop (surprise!!!) that makes the
        // required calculations and draws to the screen (renders). More
        // complicated games use multiple loops (on different threads)for
        // different tasks, such as splitting up calculations and rendering. For
        // example, my particle simulator has 3 different loops running on
        // multiple threads.
        Timer timer = new Timer();

        // Preface to the next little part:
        // The general idea is that we tell the timer to schedule a task at a
        // fixed rate. The arguments are a TimerTask object, a delay before
        // starting the task (in milliseconds), and the time between each
        // execution of the task (in milliseconds).
        // The "new TimerTask(){...}" part is something called an anonymous
        // class, a useful concept which you can look up when you get a chance.
        // The very basic idea is that it creates an instance of a class with
        // custom behavior (and variables).
        timer.scheduleAtFixedRate(new TimerTask()
        {

            // Override the (abstract) method run() in this anonymous
            // implementation of the TimerTask class. We do this to create the
            // functionality we want.
            @Override
            public void run()
            {
                // Send the food object to the Character class' handleFood()
                // method so that the character can determine if it ate the
                // food, and create a new one if it did. If it ate the food,
                // true is returned so we increment the score.
                if (character.handleFood(food))
                {
                    score++;
                }

                character.move(); // Calls the Character class' move() method to
                                  // make the character move

                // Calls the Character class' didCollide() method which returns
                // a boolean. If the character did collide, the method will
                // return true and the contents of the if statement will be
                // evaluated
                if (character.didCollide())
                {
                    // Sets game over variable to true
                    gameOver = true;

                    // Stops the TimerTask from running again.
                    cancel();
                }

                // Resets the keyPressed variable to false
                keyPressed = false;

                // Calls repaint(), a method Swing classes use to call the
                // paint() method with the appropriate Graphics object. I can't
                // currently think of a reason to ever call the paint() method
                // by hand, but there may be one. Regardless, I'd be shocked if
                // you had need to do so anytime in the near future.
                repaint();
            }
        }, 750, MILLI_BETWEEN_CALCS);

        // Tells the JFrame to call the methods declared in the KeyListener
        // interface overridden by this class. Interfaces are interesting, and
        // closely related to abstract classes. Ask me sometime and I'll try to
        // explain the difference. It's more conceptual than functional at this
        // point. In the meantime, do some research on OOP in Java.
        frame.addKeyListener(this);

        // Sets the background of Snake to black. setBackground() is an
        // inherited method from JPanel and takes a Color object.
        setBackground(Color.BLACK);

        // Makes the JFrame visible. Most Swing components are visible by
        // default, but not JFrames.
        frame.setVisible(true);
    }

    // This overrides the default paint method for JPanel. As I said earlier in
    // this class' comments, you really shouldn't call this directly.
    @Override
    public void paint(Graphics g)
    {
        // Calls the paint method in JPanel.
        // Basically to explain super with a simile, "super" is to "this" as
        // "JPanel" is to "Snake". If you don't know what I mean by "this", it's
        // something you should look up. I can try to explain it to you, but I'm
        // not completely sure how to word it.
        super.paint(g);

        // Casts g to a Graphics2D object. Casting is important, so you should
        // definitely research it.
        Graphics2D g2d = (Graphics2D) g;

        // Calls the render() method in the Food class, and passes it the g2d
        // object
        food.render(g2d);

        // Calls the render() method in the Character class, and passes it the
        // g2d object
        character.render(g2d);

        // If the gameOver variable is true, this executes the contents of the
        // if statement.
        // Something to note: Everything between the parentheses
        // of an if statement must evaluate to a boolean, meaning it's either
        // true or false. So, for example, 1 == 2 returns false, and 1 == 1
        // returns true. Basically, this means that == is a binary operator
        // between two primitives or objects that returns true or false
        // (identically opposite to !=, which means not equals). Another (VERY)
        // important thing about the == operator is that for primitives, such as
        // ints and chars, it compares value, but for objects, such as String,
        // it compares to see if the objects are the same object (i.e. they
        // occupy the same place in memory). With Strings in particular, this
        // can be very confusing. This is because of how the Java Virtual
        // Machine (JVM) internally deals with strings. So if you use a string
        // literal (something that starts with " and ends with "), == may work
        // how you want, but it may not. With Strings, and many other objects,
        // it is better to use the .equals() method that every object has. If
        // it's never overridden in the relevant class hierarchy, the Object
        // class (the top level class that every Java class is a decendent of)
        // defines .equals() to be identical to == anyways.
        // Also note that if you use logical and/or operators, you
        // can combine multiple booleans. For example, if you have
        // 1 == 2 && 1 == 1, you get false, but if you have 1 == 2 || 1 == 1,
        // you get true.
        if (gameOver)
        {
            // Sets the font of the g2d object to bolded Ariel of size 96.
            g2d.setFont(new Font("Ariel", Font.BOLD, 96));

            // Sets the color of the g2d object to red
            g2d.setColor(Color.RED);

            // Draws the string "Game Over" at (50,200). Note that (0,0) in
            // Swing (and many other 2 dimensional coordinate systems for
            // computer graphics) is the top left corner. y increases as you go
            // towards the bottom of the screen, and x increases as you go
            // towards the right.
            g2d.drawString("Game Over", 50, 200);
        }

        // Draw the score
        g2d.setFont(new Font("Ariel", Font.BOLD, 26));
        g2d.setColor(Color.RED);
        g2d.drawString("Score: " + score, 25, 35);
    }

    // Main method, you should know what this is, and what args means, even
    // though it's not used in this program.
    public static void main(String[] args)
    {
        // Creates a new Snake object, but doesn't assign it to a variable.
        new Snake();
    }

    // KeyListener methods

    @Override
    public void keyTyped(KeyEvent e)
    {
        // not needed. This method is for when a key is pressed and the
        // released.
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        // If the a key has already been pressed this calculation cycle, this
        // exits the method without doing anything.
        if (keyPressed)
            return;
        // Gets the "keyCode" from the KeyEvent object. You don't really need to
        // worry about the specifics right now, other than that KeyEvent defines
        // constants that are associated with each key.
        int keyCode = e.getKeyCode();

        // These if statements allow for both WASD and arrow key navigation.
        // Both types are common in games. The character's current movement
        // direction is then set appropriately.
        if (keyCode == KeyEvent.VK_UP || keyCode == KeyEvent.VK_W)
            character.setDirection(Direction.UP);
        else if (keyCode == KeyEvent.VK_DOWN || keyCode == KeyEvent.VK_S)
            character.setDirection(Direction.DOWN);
        else if (keyCode == KeyEvent.VK_RIGHT || keyCode == KeyEvent.VK_D)
            character.setDirection(Direction.RIGHT);
        else if (keyCode == KeyEvent.VK_LEFT || keyCode == KeyEvent.VK_A)
            character.setDirection(Direction.LEFT);

        // Marks keyPressed as true so this method won't do anything for the
        // rest of the calculation cycle.
        keyPressed = true;
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        // Not needed
    }

}
