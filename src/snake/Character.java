package snake;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

public class Character
{
    private int length = 3;
    private Direction direction = Direction.RIGHT;
    ArrayList<CharacterPiece> characterPieces = new ArrayList<CharacterPiece>();
    private boolean mustAddPiece = false;

    public Character()
    {
        int x = Snake.CENTER_X / Snake.GRID_BLOCK_SIZE * Snake.GRID_BLOCK_SIZE + Snake.GRID_BLOCK_SIZE / 2;
        int y = Snake.CENTER_Y / Snake.GRID_BLOCK_SIZE * Snake.GRID_BLOCK_SIZE + Snake.GRID_BLOCK_SIZE / 2;
        for (int i = 0; i < length; i++)
            characterPieces.add(new CharacterPiece(x -= Snake.GRID_BLOCK_SIZE, y));
    }

    public boolean handleFood(Food food)
    {
        if (characterPieces.get(0).location.equals(food.location))
        {
            mustAddPiece = true;
            food.newFood();
            return true;
        }
        return false;
    }

    public void setDirection(Direction direction)
    {
        if (direction.getOpposite() != this.direction)
            this.direction = direction;
    }
    
    public boolean didCollide()
    {
        return didCollideWithTail() || didCollideWithWall();
    }

    private boolean didCollideWithTail()
    {
        for (int i = 1; i < characterPieces.size(); i++)
            if (characterPieces.get(0).intersectsWith(characterPieces.get(i)))
                return true;
        return false;
    }

    private boolean didCollideWithWall()
    {
        Point headLocation = getHead().location;
        if (headLocation.x < 0|| headLocation.x > Snake.WIDTH || headLocation.y  < 0 || headLocation.y  > Snake.HEIGHT - 25)
            return true;
        return false;
    }

    public void move()
    {
        int start = length - 1;
        if (mustAddPiece)
            addPiece();
        for (int i = start; i > 0; i--)
            characterPieces.get(i).moveTo(characterPieces.get(i - 1));
        characterPieces.get(0).move(direction);

    }

    public void render(Graphics2D g2d)
    {
        g2d.setColor(Color.WHITE);
        for (CharacterPiece piece : characterPieces)
            piece.render(g2d);
    }

    private void addPiece()
    {
        Point location = characterPieces.get(length - 1).location;
        ++length;
        characterPieces.add(new CharacterPiece(location));
        mustAddPiece = false;
    }

    private CharacterPiece getHead()
    {
        return characterPieces.get(0);
    }

    private class CharacterPiece
    {
        static final int RADIUS = Snake.GRID_BLOCK_SIZE / 2;
        Point location;

        public CharacterPiece(Point location)
        {
            this.location = new Point(location);
        }

        public CharacterPiece(int x, int y)
        {
            location = new Point(x, y);
        }

        boolean intersectsWith(CharacterPiece piece)
        {
            if (piece.location == null)
                System.out.println("other is null");
            else if (location == null)
                System.out.println("this is null");
            return piece.location.equals(location);
        }

        void render(Graphics2D g2d)
        {
            g2d.fillOval(location.x - RADIUS, location.y - RADIUS, RADIUS * 2, RADIUS * 2);
        }

        void move(Direction direction)
        {
            location.setLocation(location.x + direction.xModifier * Snake.GRID_BLOCK_SIZE,
                    location.y + direction.yModifier * Snake.GRID_BLOCK_SIZE);
        }

        void moveTo(CharacterPiece otherPiece)
        {
            location.setLocation(otherPiece.location);
        }
    }

}
