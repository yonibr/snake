package snake;


public enum Direction
{
    LEFT(-1,0),
    RIGHT(1,0),
    UP(0,-1),
    DOWN(0,1);

    final int xModifier,yModifier;
    private Direction(int xModifier, int yModifier)
    {
        this.xModifier = xModifier;
        this.yModifier = yModifier;
        
    }
    
    Direction getOpposite()
    {
        switch (this)
        {
            case LEFT:
                return RIGHT;
            case RIGHT:
                return LEFT;
            case UP:
                return DOWN;
            case DOWN:
                return UP;
            default:
                return null;
        }
    }
}