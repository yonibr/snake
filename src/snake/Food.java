package snake;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.Random;

public class Food
{
    private static final int SIDE = Snake.GRID_BLOCK_SIZE / 2;
    private static final int HALF_SIDE = SIDE / 2;
    private static final int CENTER_OFFSET = Snake.GRID_BLOCK_SIZE / 2;
    static private Random random = new Random();
    Point location = null;

    public Food(int x, int y)
    {
        location = getPointOnGrid(x, y);
    }

    public static Food createFood()
    {
        int x = random.nextInt(Snake.WIDTH - 100) + 50;
        int y = random.nextInt(Snake.HEIGHT - 100) + 50;
        return new Food(x, y);
    }

    public void newFood()
    {
        int x = random.nextInt(Snake.WIDTH - 100) + 50;
        int y = random.nextInt(Snake.HEIGHT - 100) + 50;
        location = getPointOnGrid(x, y);
    }

    public void render(Graphics2D g2d)
    {
        g2d.setColor(Color.GREEN);
        g2d.fillRect(location.x - HALF_SIDE, location.y - HALF_SIDE, SIDE, SIDE);
    }
    
    private Point getPointOnGrid(int x, int y)
    {
        x = x / Snake.GRID_BLOCK_SIZE * Snake.GRID_BLOCK_SIZE + CENTER_OFFSET;
        y = y / Snake.GRID_BLOCK_SIZE * Snake.GRID_BLOCK_SIZE + CENTER_OFFSET;
        return new Point(x, y);
    }

}
